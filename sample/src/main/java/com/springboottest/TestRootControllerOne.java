package com.springboottest;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.lang.reflect.Type;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.testcontroller.TestEntityOne;

@RestController
@EnableAutoConfiguration
@ComponentScan({ "com.testcontroller"})
public class TestRootControllerOne {
	
	private ArrayList<TestEntityOne> teoList;
	
	@RequestMapping("/")
	public String getData(){
		return "Hello";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/json")
	public List<ArticlePriceResponse> getJson(){
		Gson gson = new Gson();
		List<ArticlePriceResponse> list;
		Type type = new TypeToken<List<ArticlePriceResponse>>(){}.getType();
		Reader reader = new InputStreamReader(TestRootControllerOne.class.getResourceAsStream("/articleprice.json"));
		list = (List<ArticlePriceResponse>) gson.fromJson(reader, type);
		return list;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/jsonVal")
	public ArticlePriceResponse getArticleVal(@RequestParam("id") String id){
		Gson gson = new Gson();
		List<ArticlePriceResponse> list;
		Type type = new TypeToken<List<ArticlePriceResponse>>(){}.getType();
		Reader reader = new InputStreamReader(TestRootControllerOne.class.getResourceAsStream("/articleprice.json"));
		list = (List<ArticlePriceResponse>) gson.fromJson(reader, type);
		for(ArticlePriceResponse o:list){
			if((o.getArticleId().equalsIgnoreCase(id))){
				return o;
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/json/{id}")
	public ArticlePriceResponse getArticle(@PathVariable("id") String id){
		Gson gson = new Gson();
		List<ArticlePriceResponse> list;
		Type type = new TypeToken<List<ArticlePriceResponse>>(){}.getType();
		Reader reader = new InputStreamReader(TestRootControllerOne.class.getResourceAsStream("/articleprice.json"));
		list = (List<ArticlePriceResponse>) gson.fromJson(reader, type);
		for(ArticlePriceResponse o:list){
			if((o.getArticleId().equalsIgnoreCase(id))){
				return o;
			}
		}
		return null;
	}
	public ArrayList<TestEntityOne> getTeoList() {
		return teoList;
	}
	
    @Autowired
	public void setTeoList(ArrayList<TestEntityOne> teoList) {
		this.teoList = teoList;
	}

	@RequestMapping("/test")
	public HashMap<String,ArrayList<TestEntityOne>> getTest(){
		HashMap<String,ArrayList<TestEntityOne>> map = new HashMap<String,ArrayList<TestEntityOne>>();
		map.put("one", teoList);
		map.put("two", teoList);
		map.put("three", teoList);
		map.put("four", teoList);
		return map;
	}
}
